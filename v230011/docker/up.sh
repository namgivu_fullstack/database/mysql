SH=`realpath $(dirname ${BASH_SOURCE:-$0})`

params() {
    set -a; source "$SH/buildtime.env"
    #   -a broadcast all defined vars as envvar
        [[ -z $MYSQL_ROOT_PASSWORD ]] && (echo 'Envvar MYSQL_ROOT_PASSWORD is required'; kill $$)
        [[ -z $MYSQL_DATABASE      ]] && (echo 'Envvar MYSQL_DATABASE is required'; kill $$)

                               dbseed_sql_f="$SH/../dbseed/"db.sql
    dbseed_sql_f__incontainer='/docker-entrypoint-initdb.d/'db.sql
    #                          /docker-entrypoint-initdb.d  ref. https://hub.docker.com/_/mysql  > will execute files with extensions .sh, .sql and .sql.gz that are found in /docker-entrypoint-initdb.d
    #                          /docker-entrypoint-initdb.d to run .sql dbseed script in this folder ref. https://hub.docker.com/_/mysql  > Initializing a fresh instance
} ; params


docker-compose -f"$SH/docker-compose.yml" -p$p up --build -d
#                                                 --build forced recreate the container to refresh the envvar


waittillready() {
    verbose_debug=$1
    echo; printf 'Wait till container ready ...'
        while true; do
                                       status=`docker-compose -f"$SH/docker-compose.yml" -p$p exec db_c  mysql -e 'status'  -p"$MYSQL_ROOT_PASSWORD" $MYSQL_DATABASE` ; [[ $verbose_debug -eq 1 ]] && echo $status
                    check_ready=`echo $status | grep ERROR`  # if not ready, status will be like >  ERROR 2002 (HY000): Can't connect to local MySQL server through socket '/var/run/mysqld/mysqld.sock' (2)
            [[ -z "$check_ready" ]] && break

            printf '.'; sleep .6
        done; echo
} ; waittillready 0


aftermath_check() {
    echo
    echo; docker-compose -f"$SH/docker-compose.yml" -p$p  logs 'db_c'
    echo; docker-compose -f"$SH/docker-compose.yml" -p$p  ps
    echo; docker-compose -f"$SH/docker-compose.yml" -p$p  exec db_c  mysql -e 'select * from user limit 1'  -p"$MYSQL_ROOT_PASSWORD" $MYSQL_DATABASE
    #                                                                      -e query 1 row fr user table
} ; aftermath_check
